#!/bin/bash

########## Remove Pkgs ##########
dnf remove -y \
  cups \
  cheese \
  docker-common \
  docker-selinux \
  docker-engine-selinux \
  docker-engine \
  evolution \
  gedit \
  gnome-boxes \
  gnome-contacts \
  gnome-calculator \
  gnome-calendar \
  gnome-clocks \
  gnome-todo \
  gnome-weather \
  gnome-maps \
  gnome-photos \
  libreoffice* \
  simple-scan \
  rhythmbox \
  totem
########## Remove Pkgs End ##########

########## Add Repo ##########
# Atom
rpm --import https://packagecloud.io/AtomEditor/atom/gpgkey
cp /home/eric/init_fedora30/repo_file/atom.repo /etc/yum.repos.d/

# Fedora Nvidia
# dnf config-manager --add-repo=http://negativo17.org/repos/fedora-nvidia.repo
cp /home/eric/init_fedora30/repo_file/fedora-nvidia.repo /etc/yum.repos.d/

# Fedora Spotify
# dnf config-manager --add-repo=http://negativo17.org/repos/fedora-spotify.repo
cp /home/eric/init_fedora30/repo_file/fedora-spotify.repo /etc/yum.repos.d/

# Google Chrome
cp /home/eric/init_fedora30/repo_file/google-chrome.repo /etc/yum.repos.d/

# Opera
rpm --import https://rpm.opera.com/rpmrepo.key
cp /home/eric/init_fedora30/repo_file/opera.repo /etc/yum.repos.d/

# RPMFusion
dnf install -y --nogpgcheck http://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
dnf install -y --nogpgcheck http://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Slack
dnf install -y /home/eric/init_fedora30/rpm_file/slack-3.4.2-0.1.fc21.x86_64.rpm
cp /home/eric/init_fedora30/repo_file/slack.repo /etc/yum.repos.d/

# Sublimetext
rpm -v --import https://download.sublimetext.com/sublimehq-rpm-pub.gpg
cp /home/eric/init_fedora30/repo_file/sublime-text.repo /etc/yum.repos.d/

# Virtio-Win
cp /home/eric/init_fedora30/repo_file/virtio-win.repo /etc/yum.repos.d/

# Zoom
rpm --import https://zoom.us/linux/download/pubkey
dnf install -y /home/eric/init_fedora30/rpm_file/zoom_x86_64.rpm
########## Add Repo End ##########

########## Update and Install pkgs ##########
# System update
dnf update -y
dnf upgrade -y