#!/bin/bash

# Group install
dnf groupinstall -y "C Development Tools and Libraries"
dnf groupinstall -y "Virtualization"
#dnf groupinstall -y "Security Lab"

# Install package
dnf install -y \
  ansible \
  bmon \
  exfat-utils \
  fuse-exfat \
  git \
  gitflow \
  gnome-tweak-tool \
  google-chrome-stable \
  htop \
  iotop \
  iptables-services \
  lsb \
  libguestfs-tools \
  mediawriter \
  nmap \
  ncdu \
  opera-stable \
  openssh-askpass \
  pv \
  perf \
  p7zip \
  python-pip \
  python3-wheel \
  pcp-import-iostat2pcp \
  smem \
  samba \
  sysstat \
  sublime-text \
  spotify-client \
  tmux \
  tree \
  torbrowser-launcher \
  vlc \
  vim \
  virtio-win \
  wireshark

usermod -a -G libvirt eric
usermod -a -G wireshark eric
########## Update and Install pkgs End ##########

########## Other ##########
# Disable servies
systemctl disable bluetooth cups firewalld iscsi wpa_supplicant

# Enable nested virtualization
sed -i 's/#options kvm_intel nested=1/options kvm_intel nested=1/g' /etc/modprobe.d/kvm.conf

# Grub
# sed -i 's/rhgb quiet//g' /boot/grub2/grub.cfg
########## Other End ##########

# gitconfig
cp /home/eric/init_fedora30/env_file/gitconfig /home/eric/.gitconfig
chmod 644 /home/eric/.gitconfig
chown eric.eric /home/eric/.gitconfig

cp /home/eric/init_fedora30/env_file/git.sh /etc/profile.d/
chmod 644 /etc/profile.d/git.sh
chown root.root /etc/profile.d/git.sh

# vimrc
cp /home/eric/init_fedora30/env_file/vimrc /home/eric/.vimrc
chmod 644 /home/eric/.vimrc
chown eric.eric /home/eric/.vimrc

cp /home/eric/init_fedora30/env_file/vimrc /root/.vimrc
chmod 644 /root/.vimrc
chown root.root /root/.vimrc

# iptables ruler
cp /home/eric/init_fedora30/env_file/iptables_rules.sh /tmp
bash /tmp/iptables_rules.sh
rm -f /tmp/iptables_rules.sh

# 01-sysctl.conf
cp /home/eric/init_fedora30/env_file/01-sysctl.conf /etc/sysctl.d/
sysctl -p

echo "All done!!!"