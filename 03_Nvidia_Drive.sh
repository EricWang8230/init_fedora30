#!/bin/bash

dnf install -y nvidia-driver nvidia-settings
dnf install -y akmod-nvidia
dnf install -y dkms-nvidia
dnf install -y nvidia-driver-cuda
dnf install -y cuda-devel