#!/bin/bash

# ssh_config
mkdir -p /home/eric/.ssh /home/eric/.ssh/ssh_cp /home/eric/.ssh/eric_ssh_key /home/eric/.ssh/github_ssh_key /home/eric/.ssh/gitlab_ssh_key
cp /home/eric/init_fedora30/env_file/ssh_config /home/eric/.ssh/config
chmod 644 /home/eric/.ssh/config

ssh-keygen -f /home/eric/.ssh/eric_ssh_key/eric
ssh-keygen -t rsa -b 4096 -C "ericwang8230@gmail.com" -f /home/eric/.ssh/github_ssh_key/github
ssh-keygen -t rsa -b 4096 -C "ericwang8230@gmail.com" -f /home/eric/.ssh/gitlab_ssh_key/gitlab

find /home/eric/.ssh/ -type d -exec chmod 700 -R {} \;
chown eric.eric -R /home/eric/.ssh/
