#!/bin/bash

systemctl start iptables
systemctl enable iptables

iptables -F
iptables -X
iptables -Z

iptables -t filter -A INPUT -i lo -m state --state NEW -j ACCEPT
iptables -t filter -A INPUT -i virbr0 -m state --state NEW -s 192.168.122.0/24 -j ACCEPT
iptables -t filter -A INPUT -i wlp3s0 -m state --state NEW -s 192.168.88.0/24 -j ACCEPT
iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
#iptables -t filter -A INPUT -m state --state NEW,INVALID -j DROP
iptables -P INPUT DROP

iptables-save > /etc/sysconfig/iptables